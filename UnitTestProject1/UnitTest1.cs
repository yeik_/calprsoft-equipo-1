﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CapaDominio.Entidades;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void existeUsuarioTestMethod1()
        {
            Persona persona = new Persona();

            persona.Dni = "12345678";

            Assert.IsTrue(persona.existeUsuario());
        }
        [TestMethod]
        public void esValidoStockTestMethod1()
        {
            Componente componente = new Componente();

            int val = 2;
            componente.Stock = 1;

            Assert.IsTrue(componente.esValidoStock(val));
        }
        [TestMethod]
        public void esFechaEntregaValidaTestMethod1()
        {
            Reparacion reparacion = new Reparacion();

            var date = DateTime.Today.AddDays(1);
            reparacion.FechaFinReparacion = date;

            Assert.IsTrue(reparacion.esFechaEntregaValida());
        }
        [TestMethod]
        public void esValidoCostoTotalTestMethod1()
        {
            Reparacion reparacion = new Reparacion();

            reparacion.CostoReparacion = 42;

            Assert.IsTrue(reparacion.esValidoCostoTotal());
        }
        [TestMethod]
        public void esCambiableEstadoTestMethod1()
        {
            Empleado empleado = new Empleado();

            empleado.reparacion.FechaFinReparacion = DateTime.Today.AddDays(1);
            
            Assert.IsTrue(empleado.esCambiableEstado());
        }
        [TestMethod]
        public void esEdadAceptableTestMethod1()
        {
            Empleado empleado = new Empleado();

            empleado.Edad = 25;

            Assert.IsTrue(empleado.esEdadAceptable());
        }
    }
}
