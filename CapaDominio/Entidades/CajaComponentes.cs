﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDominio.Entidades
{
    public class CajaComponentes
    {
        private int idCajaComponentes;
        private int cantidadComponentes;
        //private double costoTotalComponentes = 0.0;
        private List<Componente> componentes; 

        public CajaComponentes()
        {
            componentes = new List<Componente>();
        }

        public int IdCajaComponentes { get => idCajaComponentes; set => idCajaComponentes = value; }
        public int CantidadComponentes { get => cantidadComponentes; set => cantidadComponentes = value; }
        //public double CostoTotalComponentes { get => costoTotalComponentes; set => costoTotalComponentes = value; }
        internal List<Componente> Componentes { get => componentes; set => componentes = value; }

        public double calcularCostoTotal()
        {
            double costoTotalComponentes = 0.0;

            foreach (var item in componentes)
            {
                costoTotalComponentes += item.Precio;
            }

            return costoTotalComponentes;
        }
    }
}
