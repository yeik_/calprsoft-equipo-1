﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDominio.Entidades
{
    public class Reparacion
    {

        private DateTime fechaInicioReparacion;
        private DateTime fechaFinReparacion;
        private String descripcionProblema;
        private double costoReparacion = 20;

        private Empleado empleado;
        private CajaComponentes cajaComponentes = new CajaComponentes();

        public DateTime FechaInicioReparacion { get => fechaInicioReparacion; set => fechaInicioReparacion = value; }
        public DateTime FechaFinReparacion { get => fechaFinReparacion; set => fechaFinReparacion = value; }
        public string DescripcionProblema { get => descripcionProblema; set => descripcionProblema = value; }
        public double CostoReparacion { get => costoReparacion; set => costoReparacion = value; }

        public double calcularMontoTotal()
        {
            return cajaComponentes.calcularCostoTotal() + costoReparacion;
        }

        public Boolean esFechaEntregaValida()
        {
            return DateTime.Now <= fechaFinReparacion;
        }

        public Boolean esValidoCostoTotal()
        {
            return (costoReparacion >= 20 && costoReparacion <= 60);
        }

        /*public void cambiarEstadoEmpleado()     -------------> Aplicacion
        {
            if (empleado.esCambiableEstado()) empleado.EstadoDisponible = true;

            else empleado.EstadoDisponible = false;
        }*/
    }
}
