﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDominio.Entidades
{
    public class Persona
    {
        private int idPersona;
        private String dni;
        private String nombre;
        private String apellido;
        private String email;
        private String numeroCelular;
        private DateTime fechaNacimiento;
        private int edad;

        public int IdPersona { get => idPersona; set => idPersona = value; }
        public string Dni { get => dni; set => dni = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Email { get => email; set => email = value; }
        public string NumeroCelular { get => numeroCelular; set => numeroCelular = value; }
        public DateTime FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public int Edad { get => edad; set => edad = value; }

        /*public Boolean existeUsuario()    -------> Aplicacion
        {
            return true;
        }
        */
    }
}
