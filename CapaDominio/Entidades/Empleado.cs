﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDominio.Entidades
{
    public class Empleado : Persona
    {
        private Boolean estadoDisponible;
        public Reparacion reparacion;

        public bool EstadoDisponible { get => estadoDisponible; set => estadoDisponible = value; }

        public Boolean esCambiableEstado ()
        {
            return DateTime.Now >= reparacion.FechaFinReparacion;
        }

        public Boolean esEdadAceptable()
        {
            return  base.Edad > 22;
        }

        public Boolean esLibre()
        {
            return estadoDisponible;
        }
    }


}
