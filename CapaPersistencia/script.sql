USE [master]
GO
/****** Object:  Database [BD_Proyecto]    Script Date: 25/05/2020 10:07:24 ******/
CREATE DATABASE [BD_Proyecto]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BD_Proyecto', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLKIR\MSSQL\DATA\BD_Proyecto.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BD_Proyecto_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLKIR\MSSQL\DATA\BD_Proyecto_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [BD_Proyecto] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BD_Proyecto].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BD_Proyecto] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BD_Proyecto] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BD_Proyecto] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BD_Proyecto] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BD_Proyecto] SET ARITHABORT OFF 
GO
ALTER DATABASE [BD_Proyecto] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BD_Proyecto] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BD_Proyecto] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BD_Proyecto] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BD_Proyecto] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BD_Proyecto] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BD_Proyecto] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BD_Proyecto] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BD_Proyecto] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BD_Proyecto] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BD_Proyecto] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BD_Proyecto] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BD_Proyecto] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BD_Proyecto] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BD_Proyecto] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BD_Proyecto] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BD_Proyecto] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BD_Proyecto] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BD_Proyecto] SET  MULTI_USER 
GO
ALTER DATABASE [BD_Proyecto] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BD_Proyecto] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BD_Proyecto] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BD_Proyecto] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BD_Proyecto] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BD_Proyecto] SET QUERY_STORE = OFF
GO
USE [BD_Proyecto]
GO
/****** Object:  Table [dbo].[CajaComponentes]    Script Date: 25/05/2020 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CajaComponentes](
	[idCajaComponentes] [int] NOT NULL,
	[cantidad] [int] NULL,
	[costoTotalDeComponentes] [float] NULL,
	[fk_idComponente] [int] NULL,
 CONSTRAINT [PK_CajaComponentes] PRIMARY KEY CLUSTERED 
(
	[idCajaComponentes] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clientee]    Script Date: 25/05/2020 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientee](
	[direccionDelivery] [varchar](150) NULL,
	[idPersona] [int] NOT NULL,
	[fk_DispositivoElectronico] [int] NULL,
 CONSTRAINT [PK_Clientee] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Componente]    Script Date: 25/05/2020 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Componente](
	[idComponente] [int] NOT NULL,
	[nombre] [varchar](220) NULL,
	[precio] [float] NULL,
	[tipo] [varchar](125) NULL,
	[marca] [varchar](225) NULL,
	[stock] [int] NULL,
 CONSTRAINT [PK_Componente] PRIMARY KEY CLUSTERED 
(
	[idComponente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DispositivoElectronico]    Script Date: 25/05/2020 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DispositivoElectronico](
	[idDispositivoElectronico] [int] NOT NULL,
	[nombreDispositivo] [varchar](220) NULL,
	[estadoDispositivo] [bit] NULL,
	[modelo] [varchar](225) NULL,
	[marca] [varchar](225) NULL,
	[fk_idReparacion] [int] NULL,
 CONSTRAINT [PK_DispositivoElectronico] PRIMARY KEY CLUSTERED 
(
	[idDispositivoElectronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 25/05/2020 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[estadoDisponible] [bit] NULL,
	[idPersona] [int] NOT NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Persona]    Script Date: 25/05/2020 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Persona](
	[nombre] [varchar](200) NULL,
	[apellido] [varchar](200) NULL,
	[edad] [int] NULL,
	[dni] [char](8) NULL,
	[email] [varchar](50) NULL,
	[numeroCelular] [char](8) NULL,
	[fechaNacimiento] [date] NULL,
	[idPersona] [int] NOT NULL,
 CONSTRAINT [PK_Persona] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reparacion]    Script Date: 25/05/2020 10:07:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reparacion](
	[idReparacion] [int] NOT NULL,
	[fechaInicioReparacion] [date] NULL,
	[descripcionProblema] [varchar](550) NULL,
	[fechaFinReparacion] [date] NULL,
	[costoDeReparacion] [float] NULL,
 CONSTRAINT [PK_Reparacion] PRIMARY KEY CLUSTERED 
(
	[idReparacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CajaComponentes]  WITH CHECK ADD  CONSTRAINT [FK_CajaComponentes_Componente] FOREIGN KEY([fk_idComponente])
REFERENCES [dbo].[Componente] ([idComponente])
GO
ALTER TABLE [dbo].[CajaComponentes] CHECK CONSTRAINT [FK_CajaComponentes_Componente]
GO
ALTER TABLE [dbo].[CajaComponentes]  WITH CHECK ADD  CONSTRAINT [FK_CajaComponentes_Reparacion] FOREIGN KEY([idCajaComponentes])
REFERENCES [dbo].[Reparacion] ([idReparacion])
GO
ALTER TABLE [dbo].[CajaComponentes] CHECK CONSTRAINT [FK_CajaComponentes_Reparacion]
GO
ALTER TABLE [dbo].[Clientee]  WITH CHECK ADD  CONSTRAINT [FK_Clientee_DispositivoElectronico] FOREIGN KEY([fk_DispositivoElectronico])
REFERENCES [dbo].[DispositivoElectronico] ([idDispositivoElectronico])
GO
ALTER TABLE [dbo].[Clientee] CHECK CONSTRAINT [FK_Clientee_DispositivoElectronico]
GO
ALTER TABLE [dbo].[Clientee]  WITH CHECK ADD  CONSTRAINT [FK_Clientee_Persona] FOREIGN KEY([idPersona])
REFERENCES [dbo].[Persona] ([idPersona])
GO
ALTER TABLE [dbo].[Clientee] CHECK CONSTRAINT [FK_Clientee_Persona]
GO
ALTER TABLE [dbo].[DispositivoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_DispositivoElectronico_Reparacion] FOREIGN KEY([fk_idReparacion])
REFERENCES [dbo].[Reparacion] ([idReparacion])
GO
ALTER TABLE [dbo].[DispositivoElectronico] CHECK CONSTRAINT [FK_DispositivoElectronico_Reparacion]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_Persona] FOREIGN KEY([idPersona])
REFERENCES [dbo].[Persona] ([idPersona])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_Persona]
GO
ALTER TABLE [dbo].[Reparacion]  WITH CHECK ADD  CONSTRAINT [FK_Reparacion_Empleado] FOREIGN KEY([idReparacion])
REFERENCES [dbo].[Empleado] ([idPersona])
GO
ALTER TABLE [dbo].[Reparacion] CHECK CONSTRAINT [FK_Reparacion_Empleado]
GO
USE [master]
GO
ALTER DATABASE [BD_Proyecto] SET  READ_WRITE 
GO
