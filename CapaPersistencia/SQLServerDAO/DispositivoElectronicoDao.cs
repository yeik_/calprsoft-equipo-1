﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CapaDominio.Contratos;
using CapaDominio.Entidades;

namespace Solucion_Gestion_Reparacion.CapaPersistencia.SQLServerDAO
{
    public class DispositivoElectronicoDao : IDispositivoElectronicoDao
    {
        private GestorDAO gestorDAO;

        public DispositivoElectronicoDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }
        public void guardar(DispositivoElectronico dispositivoElectrinco)
        {
            string insertarPagoSQL;

            insertarPagoSQL = "insert into Pago(nombreDispositivo, modelo, marca, estadoDispositivo) " +
                              "values(@nombreDispositivo, @modelo, @marca, @estadoDispositivo)";

            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(insertarPagoSQL);

                //comando.Parameters.AddWithValue("@idCuota_PK_FK", dispositivoElectrinco.Cuota.IdCuota);
                comando.Parameters.AddWithValue("@nombreDispositivo", dispositivoElectrinco.NombreDispositivo);
                comando.Parameters.AddWithValue("@modelo", dispositivoElectrinco.Modelo);
                comando.Parameters.AddWithValue("@marca", dispositivoElectrinco.Marca);
                comando.Parameters.AddWithValue("@estadoDispositivo", dispositivoElectrinco.EstadoDispositivo);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar guardar el dispositivo", err);
            }
        }
    }
}
